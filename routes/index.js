var express = require('express');
var router = express.Router();
let db = require('../models/index');

router.get('/getmural/:uniqueid', async function (req, res, next) {
  try {
    let uniqueid = req.params.uniqueid;

    let mural = await db.Mural.findOne({
      where: {
        uniqueid: uniqueid
      },
      include: [
        {
          model: db.Midia,
          required: false
        },
        {
          model: db.Comentario,
          required: false
        }
      ]
    });
    res.status(200).send({ Mural: mural });
  } catch (err) {
    res.status(400).send({
      error: 'Erro ao obter o mural',
      message: err.message
    });
  }
});

router.post('/comentar', async function (req, res, next) {
  try {
    let comentario = req.body.comentario
    let uniqueid = req.body.uniqueid
    let nome = req.body.nome

    let mural = await db.Mural.findOne({
      where: {
        uniqueid: uniqueid
      }
    });

    if (mural) {
      let comentarioObj = {
        nome: nome,
        comentario: comentario,
        MuralId: mural.id
      }

      await db.Comentario.create(comentarioObj);
    }else{
      res.status(400).send({ erro: 'Esse mural não existe!' });
    }

    res.status(200).send({ mural: mural });
  } catch (err) {
    res.status(400).send({
      error: 'Erro ao obter o mural',
      message: err.message
    });
  }
});

module.exports = router;
