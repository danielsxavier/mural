module.exports = function(sequelize, DataTypes) {
  let Mural = sequelize.define('Mural', {
    uniqueId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    nome: {
      type: DataTypes.STRING,
      allowNull: true
    },
    tipo: {
      type: DataTypes.ENUM('Praça', 'Estátua', 'Igreja', 'Monumento'),
      allowNull: false
    }
  }, {
    freezeTableName: true,
    classMethods: {
      associate: function(models) {
        Mural.hasMany(models.Midia, {
            foreignKey: {
              allowNull: true
            }
        });
        Mural.hasMany(models.Comentario, {
            foreignKey: {
                allowNull: true
            }
        });
      }
    }
  });
  return Mural;
};
