module.exports = function(sequelize, DataTypes) {
  let Midia = sequelize.define('Midia', {
    nome: {
      type: DataTypes.STRING,
      allowNull: true
    },
    valor: {
      type: DataTypes.STRING,
      allowNull: false  
    },
    tipo: {
      type: DataTypes.ENUM('youtube', 'texto', 'imagem', 'audio', 'video'),
      allowNull: false
    }
  }, {
    freezeTableName: true,
    classMethods: {
      associate: function(models) {
        Midia.belongsTo(models.Mural)
      }
    }
  });
  return Midia;
};
