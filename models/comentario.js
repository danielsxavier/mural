module.exports = function(sequelize, DataTypes) {
  let Comentario = sequelize.define('Comentario', {
    nome: {
      type: DataTypes.STRING,
      allowNull: true
    },
    comentario: {
      type: DataTypes.STRING,
      allowNull: false  
    }
  }, {
    freezeTableName: true,
    classMethods: {
      associate: function(models) {
        Comentario.belongsTo(models.Mural)
      }
    }
  });
  return Comentario;
};
