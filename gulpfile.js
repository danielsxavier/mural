const gulp = require('gulp');
var install = require("gulp-install");
var nodemon = require('gulp-nodemon');
var path = require("path");
var models = require("./models");
const shell = require('gulp-shell');

//Inicialização do projeto
gulp.task('install', function () {
    gulp.src(['./package.json'])
        .pipe(install());
});

//Desenvolvimento
/**
 * Gera as tabelas no banco da dados de acordo com os models,
 * se estas ainda não existirem
 */
gulp.task('sequelize', function () {
	return models.sequelize.sync({
		force: false
	});
});

/**
 * Dropa todas as as tabelas do banco e depois
 * gera tabelas para todos os models configurados
 */
gulp.task('sequelize:drop', function () {
	return models.sequelize.sync({
		force: true
	});
});

gulp.task('buildprod', shell.task('ng build --env=prod --bh "/desenv/packeat/"'));

gulp.task('nodemon', function () {
    nodemon({
        nodeArgs : ['--inspect'],
        script: 'server/bin/www'
        , ext: 'js html'
        , env: { 'NODE_ENV': 'development' }
    })
});

//gulp.task('dev', ['db', 'nodemon']);
gulp.task('default');
